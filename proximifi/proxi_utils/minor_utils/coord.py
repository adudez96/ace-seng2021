# Collection of classes for simplifying coordinates and views
__author__ = ["Aman Singh", "Zheng Hon Seah"]

# Object for latitude and longitude coordinates
class Coord:
    lat = 0.0
    lng = 0.0

    def __init__(self, latitude=0.0, longitude=0.0):
        self.lat = latitude
        self.lng = longitude

    def __str__(self):
        return "(" + str(self.lat) + "," + str(self.lng) + ")"