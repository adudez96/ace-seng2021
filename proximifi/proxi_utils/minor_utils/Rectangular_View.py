from proxi_utils.minor_utils.coord import Coord
import math


# Object to simplify rectangular view for passing
class Rectangle_View:
    top_left = Coord()
    top_right = Coord()
    bot_left = Coord()
    bot_right = Coord()

    def __init__(self, bl=Coord(), tr=Coord()):
        self.top_left = Coord(tr.lat, bl.lng)
        self.top_right = tr
        self.bot_left = bl
        self.bot_right = Coord(bl.lat, tr.lng)

    def get_centre(self):
        return Coord((self.top_left.lat + self.bot_right.lat) / 2, (self.top_left.lng + self.top_right.lng) / 2)

    # return distance between centre and top left corner
    def get_max_radius(self):
        ctr = self.get_centre()
        earthRadius = 6378137  # in metres
        latDiff = (self.top_left.lat - ctr.lat) * math.pi / 180
        lngDiff = (ctr.lng - self.top_left.lng) * math.pi / 180
        a = math.sin(latDiff / 2) * math.sin(latDiff / 2) + math.cos(self.top_left.lat * math.pi / 180) * \
                                                            math.cos(ctr.lat * math.pi / 180) * math.sin(
            lngDiff / 2) * math.sin(lngDiff / 2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        d = earthRadius * c
        return d

    @property
    def __str__(self):
        return "NE: " + str(self.top_right) + "\n" + "SW: " + str(self.bot_left)

