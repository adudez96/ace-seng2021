# INPUT:   four corners (lat/longs)
# OUTPUT:  a bunch of lat/longs representing points

from proxi_utils.minor_utils.coord import Coord


def getHotels(tl, tr, bl, br):
    cntrlat = (tl.lat + br.lat) / 2
    cntrlong = (tl.lng + br.lng) / 2

    cntr = Coord(cntrlat, cntrlong)

    return cntr