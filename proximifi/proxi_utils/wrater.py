# Wrapper for Proximifi rating system

from proxi_utils.minor_utils.Rectangular_View import Rectangle_View
from proxi_utils.minor_utils.coord import Coord
from proxi_utils.GoogleWrapper import PlacesWrapper
import math
import time


# Given a rectangle's four corners (i.e. view), find and rate
# all hotels within the rectangle, rate them and return a
# list of the information found and determined.
def rate_in_box(vw=Rectangle_View(), interests=["doctor", "grocery", "park"]):

    numinterests = 0
    for i in interests:
        if i != '':
            numinterests += 1

    # get all hotels
    hotels = get_hotels(vw)
    for h in hotels:
        h['proximifi_rating'] = 0

    # get centre of view
    centre = vw.get_centre()
    # find all POIs close to that centre that match all interests
    # interests = ["restaurant", "hairdresser", "museum"]
    points_of_interest = []
    interestno = 0
    for i in interests:
        if i == '':
            continue
        interestno += 1

        results, npt = PlacesWrapper.nearby_search(location=[centre.lat, centre.lng], keyword=i, rankby='distance')
        print("NUM POIs = " + str(len(results)))
        # rate each hotel
        for h in hotels:

            # find location coordinates of hotel
            c = Coord(float(h['geometry']['location']['lat']), float(h['geometry']['location']['lng']))

            # initialise rating stuff
            rate_bracket = [200, 500, 1000, 2000, 3500, 5000, 7500, 10000, 25000, 50000]  # unit = metres
            counts = {}
            for br in rate_bracket:
                counts[str(br)] = 0

            # count all POIs into brackets
            h['pois_' + str(interestno)] = []
            for poi in results:
                temp = poi['geometry']['location']
                temp['place_id'] = poi['place_id']
                temp['name'] = poi['name']
                h['pois_' + str(interestno)].append(temp)

                # find location coordinates of POI
                p = Coord(float(poi['geometry']['location']['lat']), float(poi['geometry']['location']['lng']))
                # calculate distance from POI to hotel
                distance = dist_in_m(c, p)

                # count into bracket
                for br in rate_bracket:
                    if distance < br:
                        counts[str(br)] += 1
                        # rate hotel

            j = 0
            while (j < len(rate_bracket)) and (counts[str(rate_bracket[j])] == 0):
                j += 1

            scaler = 0
            a = 1/81

            # print("****************************")
            rating = 0
            while j < 9:
                thisscale = -a * ((j**2) - 100)
                # print("SCALE: " + str(thisscale))
                # print("RATIO: " + str(counts[str(rate_bracket[j])]/20))
                rating += (counts[str(rate_bracket[j])]/20) * thisscale
                j += 1
                scaler += thisscale

            if scaler != 0:
                rating *= 100/scaler

            if rating >= 100:
                rating = 74

            h['rating_' + str(interestno)] = rating
            h['proximifi_rating'] += rating/numinterests

    return hotels


# Given a rectangle's four corners (i.e. view),
# find all hotels within the rectangle.
def get_hotels(vw=Rectangle_View()):
    results = []
    r = vw.get_max_radius()

    # do a search using the max radius
    hotels = PlacesWrapper.radar_search(location=[vw.get_centre().lat, vw.get_centre().lng],
                                            radius=r, types="lodging")

    # If the number of results returned is equal to 200, split the rectangle view into 2 halves,
    # because we can't be sure if the area has more than 200 hotels
    if len(hotels) == 200:
        if vw.bot_right.lng - vw.top_left.lng >= vw.top_left.lat - vw.bot_right.lat:
            newVw1 = Rectangle_View(vw.bot_left, Coord(vw.top_right.lat, vw.get_centre().lng))
            newVw2 = Rectangle_View(Coord(vw.bot_right.lat, vw.get_centre().lng), vw.top_right)
        else:
            newVw1 = Rectangle_View(Coord(vw.get_centre().lat, vw.top_left.lng), vw.top_right)
            newVw2 = Rectangle_View(vw.bot_left, Coord(vw.get_centre().lat, vw.bot_right.lng))
        results.extend(get_hotels(newVw1))
        results.extend(get_hotels(newVw2))
        results = remove_duplicates(results)

    # Otherwise the number of results returned is less than 200, we know that the number of hotels
    # in the rectangle must be equal to the number of results returned
    else:

        # remove all the hotels that are outside of the rectangle,
        # since the search area is a circle
        i = 0
        while i < len(hotels):
            if (hotels[i]['geometry']['location']['lat'] > vw.top_left.lat or
                        hotels[i]['geometry']['location']['lat'] < vw.bot_right.lat or
                        hotels[i]['geometry']['location']['lng'] > vw.bot_right.lng or
                        hotels[i]['geometry']['location']['lng'] < vw.top_left.lng):
                del hotels[i]
            else:
                i += 1
        results = hotels

    return results


# remove duplicates in results list
def remove_duplicates(l):
    new_l = []
    seen = {}
    for item in l:
        if item["id"] in seen:
            continue
        seen[item["id"]] = 1
        new_l.append(item)
    return new_l


# given two lat longs, find the distance in m between them
def dist_in_m(p, q):
    dc = math.sqrt((p.lat - q.lat)**2 + (p.lng - q.lng)**2)
    dist = dc * 110000
    return dist