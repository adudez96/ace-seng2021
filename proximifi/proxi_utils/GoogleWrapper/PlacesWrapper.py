# Wrapper for Google Places API
__author__ = ["Aman Singh", "Zhen Hong Seah"]

from proxi_utils.minor_utils.Rectangular_View import Rectangle_View
from proxi_utils.minor_utils.coord import Coord
from urllib.request import urlopen
import json
from urllib.parse import quote

api_key = "AIzaSyBz2scvAGpTaXwFpJ3AAK_iBNLU5htaE4I"


# locations within radius which fulfil tag
def radar_search(location=[0, 0], radius=None, keyword=None, minprice=None,
                 maxprice=None, name=None, opennow=None, types=None, zagatselected=None):
    # print("radar called... Location:" + str(location[0]) + ", " + str(location[1]))
    arguments = locals()
    results = []
    url = "https://maps.googleapis.com/maps/api/place/radarsearch/json?key=" + api_key

    for arg, value in arguments.items():
        if arg == 'location':
            url += "&" + arg + "=" + str(value[0]) + "," + str(value[1])
        elif value is not None:
            url += "&" + arg + "=" + str(value)

    results = urlopen(url)
    string = results.read().decode('utf-8')
    json_obj = json.loads(string)

    return json_obj.get('results')


# Given a list of keywords, return a list of locations which
#   fulfil the keywords
def text_search(query="", location=None, radius=None, language=None, minprice=None,
                maxprice=None, opennow=None, types=None, pagetoken=None, zagatselected=None):
    arguments = locals()
    results = []
    url = "https://maps.googleapis.com/maps/api/place/textsearch/json?key=" + api_key

    for arg, value in arguments.items():
        if value is not None:
            if arg == 'location':
                url += "&" + arg + "=" + str(value[0]) + "," + str(value[1])
            else:
                url += "&" + arg + "=" + str(value)

    results = urlopen(url)
    string = results.read().decode('utf-8')
    json_obj = json.loads(string)
    return json_obj.get('results')


def nearby_search(location=[0, 0], radius=None, rankby=None, keyword=None, language=None,
                  minprice=None, maxprice=None, name=None, opennow=None, pagetoken=None, types=None):
    # if pagetoken is not None:
    #     print("!!!!!" + pagetoken)
    args = locals()
    results = []
    query = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?&key=" + api_key

    rankbydistance = False

    for arg, value in args.items():
        if arg == 'location':
            if pagetoken is None:
                query += "&" + arg + "=" + str(value[0]) + "," + str(value[1])
        elif value is not None:
            query += "&" + arg + "=" + str(value)

    # if pagetoken is not None:
    #     print("*******************************")
    #     print(query)
    #     print("*******************************")

    result = urlopen(query)
    string = result.read().decode('utf-8')
    jsonobj = json.loads(string)
    # if pagetoken is not None:
    #     print(string)
    pagetoken = jsonobj.get('next_page_token')
    return jsonobj.get('results'), pagetoken


# Given an address, return a lat/long
def geocode_ad2coord(address=None, route=None, locality=None, administrative_area=None, postal_code=None,
                     country=None, bounds=None, language=None, region=None):
    arguments = locals()
    results = []
    url = "https://maps.googleapis.com/maps/api/geocode/json?key=" + api_key
    components = "&components="
    isFirstComponent = True

    for arg, value in arguments.items():
        if value is not None:
            if (arg == 'route' or arg == 'locality' or arg == 'administrative_area' or
                        arg == 'postal_code' or arg == 'country'):
                if isFirstComponent == False:
                    components += "|"
                else:
                    isFirstComponent = False
                components += arg + ":" + value.__str__
            elif arg == 'address':
                url += "&" + arg + "=" + quote(value.__str__)  # quote() replaces spaces in url with %20
            elif arg == 'bounds':
                url += ("&" + arg + "=" + value['coord1'][0].__str__ + "," + value['coord1'][1].__str__ +
                        "|" + value['coord2'][0].__str__ + "," + value['coord2'][1].__str__)
            else:
                url += "&" + arg + "=" + value.__str__

    # if components is not empty
    if isFirstComponent == False:
        url += components

    results = urlopen(url)
    string = results.read().decode('utf-8')
    json_obj = json.loads(string)
    return json_obj


# Given a lat/long, return address
def geocode_coord2ad(latlng=None, place_id=None, language=None, result_type=None, location_type=None):
    arguments = locals()
    results = []
    url = "https://maps.googleapis.com/maps/api/geocode/json?key=" + api_key

    for arg, value in arguments.items():
        if value is not None:
            if arg == 'latlng':
                url += "&" + arg + "=" + value[0].__str__ + "," + value[1].__str__
            elif arg == 'result_type' or arg == 'location_type':
                url += "&" + arg + "="
                for i, elem in enumerate(value):
                    if i != 0:
                        url += "|"
                    url += value[i]
            else:
                url += "&" + arg + "=" + value.__str__

    results = urlopen(url)
    string = results.read().decode('utf-8')
    json_obj = json.loads(string)
    return json_obj


# Given a Place ID or reference, return detailed info
def place_details(placeID=None, reference=None):
    url = "https://maps.googleapis.com/maps/api/place/details/json?key=" + api_key
    if reference is not None:
        url += "&reference=" + reference
    else:
        url += "&placeid=" + placeID
    results = urlopen(url)
    string = results.read().decode('utf-8')
    json_obj = json.loads(string)
    return json_obj
