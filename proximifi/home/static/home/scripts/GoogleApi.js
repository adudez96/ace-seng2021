var GoogleApi = {
    service: null,
    init: function () {
        $(document).ready(function () {
            service = new google.maps.places.PlacesService(map);
        });
    },

    placeDetails: function (request, callback) {
        service.getDetails(request, callback);
    }
};