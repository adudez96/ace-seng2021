(function () {
    GoogleMap.init();
    Forms.init();
    WindowJS.init();
    GoogleApi.init();
})();