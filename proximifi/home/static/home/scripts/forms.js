var mainFormSubmit;
var first = null;
var second = null;
var third = null;
var firstval = 0;
var secondval = 0;
var thirdval = 0;

Forms = {
    init: function () {
        Forms.bindUI();
        mainFormSubmit = true;
    },

    bindUI: function () {
        $(document).ready(function () {
            $(".newUser_inputForm").on('submit', function (event) {
                event.preventDefault();
                $('#search_bar_compressed').val($('#search_bar_main').val());
                $('#interest_1_compressed').val($('#interest_1_main').val());
                $('#interest_2_compressed').val($('#interest_2_main').val());
                $('#interest_3_compressed').val($('#interest_3_main').val());
                GoogleMap.moveTo($('.newUser_inputForm input[type=search]').val(), Forms.onFormSubmit);
                $('.header_wrapper').hide();
                $('.header_compressed_wrapper').show();
                $('.detailedView_wrapper').show();
            });
            $(".compressed_form").on('submit', function (event) {
                GoogleMap.clearInterests();
                GoogleMap.clearMap();
                event.preventDefault();
                GoogleMap.moveTo($('.compressed_form input[type=search]').val(), Forms.onFormSubmit);
            });
        });
    },

    onFormSubmit: function () {
        Forms.formTracker.init();
        Forms.resetGlobals();
        var nelatlng = map.getBounds().getNorthEast();
        var nelat = nelatlng.lat();
        var nelng = nelatlng.lng();

        var swlatlng = map.getBounds().getSouthWest();
        var swlat = swlatlng.lat();
        var swlng = swlatlng.lng();

        var km10 = (5 / 111);

        var ty = swlat;
        while (ty < nelat) {
            var tx = swlng;
            while (tx < nelng) {
                var latlngbl = new google.maps.LatLng(ty, tx);
                var latlngtr = new google.maps.LatLng(ty + km10, tx + km10);
                if (tx + km10 > nelng) {
                    latlngtr = new google.maps.LatLng(ty + km10, nelng);
                }
                if (ty + km10 > nelat) {
                    latlngtr = new google.maps.LatLng(nelat, tx + km10);
                }
                if (ty + km10 > nelat && tx + km10 > nelng) {
                    latlngtr = new google.maps.LatLng(nelat, nelng);
                }
                Forms.ajaxSubmission(latlngbl, latlngtr);
                Forms.formTracker.registerRequest()
                tx = tx + km10;
            }
            ty = ty + km10;
        }
        if (mainFormSubmit) mainFormSubmit = false;
    },

    ajaxSubmission: function (swlatlng, nelatlng) {
        var ajaxlatlng = swlatlng.lat().toFixed(6) + "," + swlatlng.lng().toFixed(6)
            + "," + nelatlng.lat().toFixed(6) + "," + nelatlng.lng().toFixed(6);
        var idAddon;
        if (mainFormSubmit) {
            idAddon = "main";
        } else {
            idAddon = "compressed";
        }
        $.ajax({
            url: "ajax/form_submit/",
            type: "GET",
            data: {
                address: $("#search_bar_" + idAddon).val(),
                interest_1: $("#interest_1_" + idAddon).val().replace(" ", "+"),
                interest_2: $("#interest_2_" + idAddon).val().replace(" ", "+"),
                interest_3: $("#interest_3_" + idAddon).val().replace(" ", "+"),
                latlng: ajaxlatlng
                //latlng : map.getBounds().toUrlValue()
            },
            dataType: "json",
            success: function (result, status, xhr) {
                Forms.formTracker.registerResponse();
                Forms.onAjaxSuccess(result, status, xhr)
            },
            error: function (xhr, errmsg, err) {
                console.log("Ajax Failed - " + errmsg);
            }

        });
    },

    //sets markers using JSON obj returned from radar search
    //**can be reused for marking POIs**
    onAjaxSuccess: function (result, status, xhr) {
        //var str = JSON.stringify(result, null, 2);
        var i = 0;
        for (i in  result) {
            var lat = result[i]['geometry']['location']['lat'];
            var lng = result[i]['geometry']['location']['lng'];
            var myLatLng = new google.maps.LatLng(lat, lng);
            var rating = result[i]['proximifi_rating'];
            var place_id = result[i]['place_id'];
            if (result[i] == first) {
                GoogleMap.createTopMarker(result[i], 1);
            } else if (result[i] == second) {
                GoogleMap.createTopMarker(result[i], 2);
            } else if (result[i] == third) {
                GoogleMap.createTopMarker(result[i], 3);
            } else {
                GoogleMap.createHotelMarker(myLatLng, result[i]);
            }
        }
    },

    resetGlobals: function () {
        first = null;
        second = null;
        third = null;
        firstval = 0;
        secondval = 0;
        thirdval = 0;
    },

    findTop: function () {
        var i = 0;
        for (i in GoogleMap.markers) {
            if (GoogleMap.markers[i]['proximifi_rating'] > firstval) {
                third = second;
                thirdval = secondval;
                second = first;
                secondval = firstval;
                first = GoogleMap.markers[i]
                firstval = GoogleMap.markers[i]['proximifi_rating'];
            } else if (GoogleMap.markers[i]['proximifi_rating'] > secondval) {
                third = second;
                thirdval = secondval;
                second = GoogleMap.markers[i];
                secondval = GoogleMap.markers[i]['proximifi_rating'];
            } else if (GoogleMap.markers[i]['proximifi_rating'] > thirdval) {
                third = GoogleMap.markers[i];
                thirdval = GoogleMap.markers[i]['proximifi_rating'];
            }
        }
        i = 0;
        for (i in  GoogleMap.markers) {
            if (GoogleMap.markers[i] == first) {
                GoogleMap.createTopMarker(GoogleMap.markers[i], 1);
            } else if (GoogleMap.markers[i] == second) {
                GoogleMap.createTopMarker(GoogleMap.markers[i], 2);
            } else if (GoogleMap.markers[i] == third) {
                GoogleMap.createTopMarker(GoogleMap.markers[i], 3);
            }
        }
    },
    /*
     This object should be used to control the loading gif for the form submission
     To start the loader call the init() method
     Use the registerRequests for a new ajax request and a registerResponse for a successful ajax response
     (ASSUMING NO ERRORS IN RESPONSE)
     The tracker will terminate when all responses have been received
     */
    formTracker: {
        ajaxRequests: 0,

        ajaxResponses: 0,

        init: function () {
            this.ajaxRequests = 0;
            this.ajaxResponses = 0;
            WindowJS.showMapLoader();
            console.time("mapSearch");
        },

        registerRequest: function () {
            this.ajaxRequests++;
        },

        registerResponse: function () {
            this.ajaxResponses++;
            if (this.ajaxRequests == this.ajaxResponses) {
                this.terminate();
            }
        },

        terminate: function () {
            WindowJS.hideMapLoader();
            Forms.findTop();
            console.timeEnd("mapSearch");
        }
    }
};
