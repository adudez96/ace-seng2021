//map.js
var map;
var lastMarker;
var GoogleMap = {
    markers: [],
    interestmarkers: [],
    container: null, //initialized in init
    geocoder: null, //initialized in init
    //placeDetailsService: null,
    settings: {
        center: {lat: -33.865143, lng: 151.209900},
        zoom: 12,
        zoomControl: true,
        mapTypeControl: false,
        scaleControl: true,
        streetViewControl: false,
        rotateControl: false,
        styles: [{
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]
        }, {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
        }, {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
        }, {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
        }, {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [{"color": "#ffffff"}, {"lightness": 16}]
        }, {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
        }, {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [{"color": "#dedede"}, {"lightness": 21}]
        }, {
            "elementType": "labels.text.stroke",
            "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
        }, {
            "elementType": "labels.text.fill",
            "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
        }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
        }, {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
        }]
    },

    init: function () {
        this.bindUI();
        $(document).ready(function () {
            GoogleMap.geocoder = new google.maps.Geocoder();
            map = new google.maps.Map(GoogleMap.container[0], GoogleMap.settings);//$('map') returns a jquery object. [0] will return the DOM object
            //GoogleMap.placeDetailsService = new google.maps.places.PlacesService(map);
            map.addListener('dragend', GoogleMap.listeners.onDragEnd);
        });
    },

    bindUI: function () {
        $(document).ready(function () {
            GoogleMap.container = $('#map_canvas')
        });
    },

    moveTo: function (address, callback) {
        this.geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.panTo(results[0].geometry.location);
                map.setZoom(14);
            } else {
                console.log('Geocode was not successful for the following reason: ' + status);
            }
            if (typeof(callback) !== 'undefined') {
                callback();
            }
        });
    },

    createHotelMarker: function (LatLng, result) {
        var marker = new google.maps.Marker({
            position: LatLng,
            proximifi_rating: result['proximifi_rating'],
            place_id: result['place_id'],
            int_1_rating: result['rating_1'],
            int_2_rating: result['rating_2'],
            int_3_rating: result['rating_3'],
            int_1_list: result['pois_1'],
            int_2_list: result['pois_2'],
            int_3_list: result['pois_3'],
        });
        var pinIcon = new google.maps.MarkerImage(
            "/static/home/images/test2.png",
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            null, /* anchor is bottom center of the scaled image */
            new google.maps.Size(25, 25)
        );

        marker.setIcon(pinIcon);
        marker.setMap(map);
        marker.addListener('click', function () {
            GoogleMap.listeners.onMarkerClick(marker);
        });
        this.markers.push(marker);
    },

    createTopMarker: function (result, rank) {
        var iconURL;
        if (rank == 1) {
            iconURL = "/static/home/images/marker_1.png"
        } else if (rank == 2) {
            iconURL = "/static/home/images/marker_2.png"
        } else if (rank == 3) {
            iconURL = "/static/home/images/marker_3.png"
        }
        var pinIcon = new google.maps.MarkerImage(
            iconURL,
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            null, /* anchor is bottom center of the scaled image */
            new google.maps.Size(30, 50)
        );

        result.setIcon(pinIcon);
        result.setMap(map);
    },

    createInterestMarker: function (marker, interest) {
        var detailsService = new google.maps.places.PlacesService(map);
        if (interest == 1) {
            for (i in marker.int_1_list) {
                $.ajax({
                    url: "ajax/place_request/",
                    type: "GET",
                    data: {
                        ID: marker.int_1_list[i]["place_id"]
                    },
                    dataType: "json",
                    success: function (result, status, xhr) {
                        var place = result.result;
                        var imarker = new google.maps.Marker({
                            position: place.geometry.location,
                            place_id: place.place_id,
                        });
                        var html = "<h1>" + place.name + "</h1>";
                        //if (place.photos) {
                        //    html += "<img src='" + place.photos[0].getUrl({'maxWidth': 100, 'maxHeight': 100}) + "'/>";
                        //}
                        if (place.website) {
                            html += "<a href='" + place.website + "' target='_blank'>View Website</a>";
                        }
                        imarker.addListener('click', function () {
                            var infowindow = new google.maps.InfoWindow();
                            infowindow.setContent(html);
                            infowindow.open(map, imarker);
                        });
                        var pinIcon = new google.maps.MarkerImage(
                            "/static/home/images/marker_orange.gif",
                            null, /* size is determined at runtime */
                            null, /* origin is 0,0 */
                            null, /* anchor is bottom center of the scaled image */
                            new google.maps.Size(15, 15)
                        );
                        imarker.setIcon(pinIcon);
                        imarker.setMap(map);
                        GoogleMap.interestmarkers.push(imarker);

                    },
                    error: function (xhr, errmsg, err) {
                        console.log(err);
                    }

                });
            }
        } else if (interest == 2) {
            for (i in marker.int_2_list) {
                $.ajax({
                    url: "ajax/place_request/",
                    type: "GET",
                    data: {
                        ID: marker.int_2_list[i]["place_id"]
                    },
                    dataType: "json",
                    success: function (result, status, xhr) {
                        var place = result.result;
                        var imarker = new google.maps.Marker({
                            position: place.geometry.location,
                            place_id: place.place_id,
                        });
                        var html = "<h1>" + place.name + "</h1>";
                        //if (place.photos) {
                        //    html += "<img src='" + place.photos[0].getUrl({'maxWidth': 100, 'maxHeight': 100}) + "'/>";
                        //}
                        if (place.website) {
                            html += "<a href='" + place.website + "' target='_blank'>View Website</a>";
                        }
                        imarker.addListener('click', function () {
                            var infowindow = new google.maps.InfoWindow();
                            infowindow.setContent(html);
                            infowindow.open(map, imarker);
                        });
                        var pinIcon = new google.maps.MarkerImage(
                            "/static/home/images/marker_green.gif",
                            null, /* size is determined at runtime */
                            null, /* origin is 0,0 */
                            null, /* anchor is bottom center of the scaled image */
                            new google.maps.Size(15, 15)
                        );
                        imarker.setIcon(pinIcon);
                        imarker.setMap(map);
                        GoogleMap.interestmarkers.push(imarker);

                    },
                    error: function (xhr, errmsg, err) {
                        console.log(err);
                    }

                });
            }
        } else if (interest == 3) {
            for (i in marker.int_3_list) {
                $.ajax({
                    url: "ajax/place_request/",
                    type: "GET",
                    data: {
                        ID: marker.int_3_list[i]["place_id"]
                    },
                    dataType: "json",
                    success: function (result, status, xhr) {
                        var place = result.result;
                        var imarker = new google.maps.Marker({
                            position: place.geometry.location,
                            place_id: place.place_id,
                        });
                        var html = "<h1>" + place.name + "</h1>";
                        //if (place.photos) {
                        //    html += "<img src='" + place.photos[0].getUrl({'maxWidth': 100, 'maxHeight': 100}) + "'/>";
                        //}
                        if (place.website) {
                            html += "<a href='" + place.website + "' target='_blank'>View Website</a>";
                        }
                        if (place.international_phone_number) {
                            html += "<p>Phone number: " + place.international_phone_number + "</p>";
                        }
                        imarker.addListener('click', function () {
                            var infowindow = new google.maps.InfoWindow();
                            infowindow.setContent(html);
                            infowindow.open(map, imarker);
                        });
                        var pinIcon = new google.maps.MarkerImage(
                            "/static/home/images/marker_blue.gif",
                            null, /* size is determined at runtime */
                            null, /* origin is 0,0 */
                            null, /* anchor is bottom center of the scaled image */
                            new google.maps.Size(15, 15)
                        );
                        imarker.setIcon(pinIcon);
                        imarker.setMap(map);
                        GoogleMap.interestmarkers.push(imarker);

                    },
                    error: function (xhr, errmsg, err) {
                        console.log(err);
                    }

                });
            }
        }
    },

    clearMap: function () {
        for (i in GoogleMap.markers) {
            GoogleMap.markers[i].setMap(null);
        }
        GoogleMap.markers = [];
    },

    clearInterests: function () {
        for (i in GoogleMap.interestmarkers) {
            GoogleMap.interestmarkers[i].setMap(null);
        }
        GoogleMap.interestmarkers = [];
    },

    listeners: {
        onDragEnd: function () {
            //this can be changed later, just temp
            $.get("/home/ajax/bound_change/", {'latlng': map.getBounds().toUrlValue()}, function (data, status) {
                console.log("The data returned was + \n" + data);
            });
        },
        onMarkerClick: function (marker) {
            lastMarker = marker;
            GoogleMap.clearInterests();
            console.log("PLACE ID - " + marker.place_id );
            $.ajax({
                url: "ajax/place_request/",
                type: "GET",
                data: {
                    ID: marker.place_id
                },
                dataType: "json",
                success: function (result, status, xhr) {
                    $('.' + lastTabWrapper + "_wrapper").hide();
                    lastTab.removeClass("shadowBox");
                    $('.overview_wrapper').show();
                    $('.overview_wrapper').addClass("shadowBox");
                    lastTabWrapper = "overview";
                    lastTab = $('.tabs_wrapper ul :nth-child(1)');
                    WindowJS.changeDetailsDescription(marker, result.result);
                },
                error: function (xhr, errmsg, err) {
                }

            });
        }
    }
};

