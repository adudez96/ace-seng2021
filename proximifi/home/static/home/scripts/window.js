var lastTabWrapper = "overview";
var lastTab = $('.tabs_wrapper ul :nth-child(1)');
WindowJS = {
    init: function () {
        WindowJS.bindUI();
    },

    bindUI: function () {
        $(document).ready(function () {
            $('.tabs_wrapper ul li').on("click", function () {
                $('.' + lastTabWrapper + "_wrapper").hide();
                lastTab.removeClass("shadowBox");
                lastTab = $(this);
                $(this).addClass("shadowBox");
                GoogleMap.clearInterests();
                switch ($(this).text()) {
                    case "Overview":
                        $('.overview_wrapper').show();
                        lastTabWrapper = "overview";
                        break;
                    case "Interest 1":
                        $('.interest1_wrapper').show();
                        lastTabWrapper = "interest1";
                        GoogleMap.createInterestMarker(lastMarker, 1);
                        break;
                    case "Interest 2":
                        $('.interest2_wrapper').show();
                        lastTabWrapper = "interest2";
                        GoogleMap.createInterestMarker(lastMarker, 2);
                        break;
                    case "Interest 3":
                        $('.interest3_wrapper').show();
                        lastTabWrapper = "interest3";
                        GoogleMap.createInterestMarker(lastMarker, 3);
                        break;
                }
            });
        });
    },

    changeDetailsDescription: function (marker, placeDetails) {
        var overviewWrapper = $('.overview_wrapper');
        $('.tabs_wrapper ul :nth-child(1)').addClass("shadowBox");
        overviewWrapper.empty();
        overviewWrapper.append("<h1 class='tab_title'></div>");
        overviewWrapper.append("<div id='div1'></div>");
        $('.overview_wrapper .tab_title').html(placeDetails.name);
        start1(marker.proximifi_rating);
        overviewWrapper.append("<div class='info_box'></div>");
        var infoBox = $('.overview_wrapper .info_box');
        infoBox.empty();
        if (placeDetails.website) {
            infoBox.append("<a href='" + placeDetails.website + "' class='info_box_text' target='_blank'>Visit website</a>");
        }

        if (placeDetails.international_phone_number) {
            infoBox.append("<p class='info_box_text'>" + placeDetails.international_phone_number + "</p>");
        }

        var rating = placeDetails.rating;
        infoBox.append("<p class='info_box_text'>User Rating:</p>");
        infoBox.append("<div class='stars_wrapper'></div");
        var starsWrapper = $('.stars_wrapper');
        starsWrapper.empty();
        var i = 0;
        for (; i < Math.floor(rating); i++) {
            starsWrapper.append("<img src='/static/home/images/star_full.gif' class='hotel_stars'>");
        }
        if (i < rating) {
            starsWrapper.append("<img src='/static/home/images/star_half.gif' class='hotel_stars'>");
            i++;
        }
        for (; i < 5; i++) {
            starsWrapper.append("<img src='/static/home/images/star_empty.gif' class='hotel_stars'>")
        }

        //INTEREST 1 TAB
        var interest1Wrapper = $('.interest1_wrapper');
        interest1Wrapper.empty();
        interest1Wrapper.append("<h1 class='tab_title'></div>");
        var idAddon;
        if (mainFormSubmit) {
            idAddon = "main";
        } else {
            idAddon = "compressed";
        }
        //interest_2: $("#interest_2_" + idAddon).val();
        //interest_3: $("#interest_3_" + idAddon).val();
        $('.interest1_wrapper .tab_title').html($("#interest_1_" + idAddon).val() || "Interest 1");
        interest1Wrapper.append("<div id='div2'></div>");
        start2(marker.int_1_rating);
        interest1Wrapper.append("<div id='info1_wrapper'></div>");
        var info1Wrapper = $('#info1_wrapper');
        var list1 = marker.int_1_list;

        //for (var i in list1) {
        //    if (i == 4 ) break;
        //    var x = parseInt(i);
        //    x = x + 1;
        //    var interestInfoWrapper = $("<div class='interest_info_wrapper'></div>");
        //    info1Wrapper.append(interestInfoWrapper);
        //    interestInfoWrapper.append("<div class='interest_number'>" + (x) + "</div>");
        //    var details = $("<div class='interest_details'></div>");
        //    interestInfoWrapper.append(details);
        //    var callback = function (place, status) {
        //        var toAdd = details;
        //        if (status == google.maps.places.PlacesServiceStatus.OK) {
        //            toAdd.append("<p class='info_box_text'>" + place.name + "</p>");
        //            if (place.website) {
        //                toAdd.append("<p class='info_box_text'>" + place.website + "</p>");
        //            }
        //        } else {
        //            console.log(status);
        //        }
        //    };
        //    console.log(list1[i]["place_id"]);
        //    GoogleApi.placeDetails({placeId: "ChIJN1t_tDeuEmsRUsoyG83frY4"}, callback);
        //}


        //INTEREST 2 tab
        var interest2Wrapper = $('.interest2_wrapper');
        interest2Wrapper.empty();
        interest2Wrapper.append("<h1 class='tab_title'></div>");
        $('.interest2_wrapper .tab_title').html($("#interest_2_" + idAddon).val() || "Interest 2");
        interest2Wrapper.append("<div id='div3'></div>");
        start3(marker.int_2_rating);

        //INTEREST 3 tab
        var interest3Wrapper = $('.interest3_wrapper');
        interest3Wrapper.empty();
        interest3Wrapper.append("<h1 class='tab_title'></div>");
        $('.interest3_wrapper .tab_title').html($("#interest_3_" + idAddon).val() || "Interest 3");
        interest3Wrapper.append("<div id='div4'></div>");
        start4(marker.int_3_rating);
    },

    showMapLoader: function () {
        $('#loadingGIF_container').show();
    },

    hideMapLoader: function () {
        $('#loadingGIF_container').hide();
    }
};