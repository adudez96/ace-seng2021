from django.conf.urls import include, url
from . import views

__author__ = 'adirishi'

extra_patterns = [
    url(r'^bound_change/$', views.ajaxRequest, name="ajax"),
    url(r'^form_submit/$', views.formSubmit, name="form"),
    url(r'^place_request/$', views.placesRequest, name="placeRequest"),
]

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^ajax/', include(extra_patterns)),
    url(r'test/', views.test, name='test'),
]
