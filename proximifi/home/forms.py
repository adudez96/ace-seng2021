from django import forms


class SearchInputForm(forms.Form):
    address = forms.CharField(label="",
                              widget=forms.TextInput(attrs={'placeholder': '  Enter either a city or an address'}))
    interest_1 = forms.CharField(label="", widget=forms.TextInput(attrs={'class': 'interest_selector',
                                                                         'placeholder': '  Select interest or enter address'}))
    interest_2 = forms.CharField(label="", widget=forms.TextInput(attrs={'class': 'interest_selector',
                                                                         'placeholder': '  Select interest or enter address'}))
    interest_3 = forms.CharField(label="", widget=forms.TextInput(attrs={'class': 'interest_selector',
                                                                         'placeholder': '  Select interest or enter address'}))
