from django.http import Http404, HttpResponse
import re
from proxi_utils.minor_utils.coord import Coord
from proxi_utils.minor_utils.Rectangular_View import Rectangle_View
from proxi_utils.wrater import rate_in_box
from proxi_utils.GoogleWrapper.PlacesWrapper import place_details
import json
# Create your views here.
from django.shortcuts import render
from .forms import SearchInputForm


def index(request):
    form = SearchInputForm()
    return render(request, 'home/index.html', {})


def ajaxRequest(request):
    return HttpResponse("Hello, content = " + request.GET.get('latlng'))


def formSubmit(request):
    coords = request.GET.get('latlng').split(",")
    sw = Coord(float(coords[0]), float(coords[1]))
    ne = Coord(float(coords[2]), float(coords[3]))
    rectangle = Rectangle_View(sw, ne)
    interests = [request.GET.get('interest_1'), request.GET.get('interest_2'),
                 request.GET.get('interest_3')]
    print(interests)
    results = rate_in_box(rectangle, interests)
    hotels = json.dumps(results)
    return HttpResponse(hotels, 'application/json')


def placesRequest(request):
    placeId = request.GET.get('ID')
    jsonOB = place_details(placeId)
    final = json.dumps(jsonOB)
    return HttpResponse(final, 'application/json')


def test(request):
    return render(request, 'home/test.html', {})
