from django.shortcuts import render

from proxi_utils import wrater
from proxi_utils.minor_utils.Rectangular_View import Rectangle_View
from proxi_utils.minor_utils.coord import Coord
from proxi_utils.GoogleWrapper.PlacesWrapper import *

# Create your views here.


def index(request):
    return render(request, 'apitrial/index.html', {})


def backendtester(request):

    cne = Coord(-33.8611091,151.2126071)
    csw = Coord(-33.8637526,151.2059107)
    view = Rectangle_View(cne, csw)

    results = wrater.rate_in_box(view)

    return render(request, 'apitrial/backendtester.html', {'view': view, 'results': results})