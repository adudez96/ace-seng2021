from django.conf.urls import url
from . import views
__author__ = 'adirishi'

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^bet/$', views.backendtester, name="Back-end tester")
]
