var map;
function initMap() {
  map = new google.maps.Map($('#map')[0], { //$('map') returns a jquery object. [0] will return the DOM object
    center: {lat: -33.865143, lng: 151.209900},
    zoom: 12
  });
}