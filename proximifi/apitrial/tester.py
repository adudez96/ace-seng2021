#!/usr/bin/env python

from proxi_utils.minor_utils.Rectangular_View import Rectangle_View
from proxi_utils.minor_utils.coord import Coord
from proxi_utils.wrater import rate_in_box

#
# view = Rectangle_View(Coord(-33.831535, 150.262220), Coord(-34.747575, 151.309685))
#   1181 results
# view = Rectangle_View(Coord(-32.831535, 150.262220), Coord(-34.847575, 151.309685))
#   199 results
view = Rectangle_View(Coord(-33.844560, 151.100564), Coord(-33.931200, 151.316857))
#
# view = Rectangle_View(Coord(-33.844560, 150.000564), Coord(-33.931200, 151.316857))

url, obj, results, rating = rate_in_box(view)
print ("total number of results is %d"%len(results))
